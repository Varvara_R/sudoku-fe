import { ReactElement, Suspense, useEffect } from 'react';
import { onCLS, onFID, onLCP } from 'web-vitals';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import './App.css';
import { SWRConfig } from 'swr';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import Spinner from './components/Spinner/Spinner';
import HomePage from './pages/HomePage/HomePage';
import UserProfile from './pages/UserProfile/UserProfile';
import GamePage from './pages/GamePage/GamePage';
import LoginPage from './pages/LoginPage/LoginPage';

function App(): ReactElement | null {
  useEffect(() => {
    window.addEventListener('unhandledrejection', async (e) => {
      await fetch('/api/events', {
        method: 'POST',
        headers: {
          Accept: 'spplication/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([
          {
            metric: { name: 'ui_errors', labels: {}, value: 1 },
            log: { level: 'error', message: e.reason },
          },
        ]),
      });
    });

    window.onload = async () => {
      await fetch('/api/events', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([
          {
            metric: { name: 'ui_page_loads', labels: {}, value: 1 },
          },
        ]),
      });
    };

    onCLS(async (cls) => {
      await fetch('/api/event', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([
          {
            metric: {
              name: 'ui_web_vitals',
              labels: { vital: 'cls' },
              value: cls.value,
            },
          },
        ]),
      });
    });
    onFID(async (fid) => {
      await fetch('/api/events', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([
          {
            metric: {
              name: 'ui_web_vitals',
              labels: { vital: 'fid' },
              value: fid.value,
            },
          },
        ]),
      });
    });
    onLCP(async (lcp) => {
      await fetch('/api/events', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([
          {
            metric: {
              name: 'ui_web_vitals',
              labels: { vital: 'lcp' },
              value: lcp.value,
            },
          },
        ]),
      });
    });
  }, []);

  return (
    <SWRConfig value={{ shouldRetryOnError: false }}>
      <BrowserRouter>
        <ErrorBoundary>
          <Suspense fallback={<Spinner />}>
            <body>
              <Header />
              <main className="container">
                <Routes>
                  <Route path="/" element={<HomePage />} />
                  <Route path="/user" element={<UserProfile />} />
                  <Route path="/newGame" element={<GamePage />} />
                  <Route path="/login" element={<LoginPage />} />
                </Routes>
              </main>
              <Footer />
            </body>
          </Suspense>
        </ErrorBoundary>
      </BrowserRouter>
    </SWRConfig>
  );
}

export default App;
