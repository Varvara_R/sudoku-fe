import React from 'react';
import styles from './HomePage.module.scss';
import image from '../../images/girl.png';
import InstructionView from '../../components/InstructionView/InstructionView';
import ContactForm from '../../components/ContactForm/ContactForm';
import CustomButton from '../../components/CustomButton/CustomButton';
import { Link } from 'react-router-dom';

function HomePage() {

  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.welcomeContainer}>
          <div className={styles.leftSide}>
            <div className={styles.title}>
              <h1>Painful joy</h1>
            </div>
            <div className={styles.description}>
              <p>Hero to zero at 60mph</p>
              <p>My eyes are getting wider with every word you say</p>
              <p>I brought a lemon to a knife fight</p>
            </div>
          </div>
          <div className={styles.image}>
            <img src={image} alt="" />
          </div>
        </div>
        <div className={styles.buttons}>
          <Link to="/newGame"><CustomButton text="New game"/></Link>
          <Link to="/login"><CustomButton text="Login" isSecondary /></Link>
        </div>
      </div>
      <InstructionView />
      <ContactForm />
    </>
  );
}

export default HomePage;
