import React, { useState } from 'react';
import styles from './GamePage.module.scss';
import { useNewGame } from '../../swr/be-data';
import Grid from '../../components/Grid/Grid';
import Modal from '../../components/Modal/Modal';
import CustomButton from '../../components/CustomButton/CustomButton';

function GamePage() {
  const { data: gameData } = useNewGame();
  const maxErrors = 3;
  const [errorsQty, setErrorsQty] = useState(0);
  const [isGameLost, setIsGameLost] = useState(false);
  const handleErrorIncrease = () => {
    if (errorsQty < 2) {
      setErrorsQty(errorsQty + 1);
    } else {
      setIsGameLost(true);
    }
  };

  return (
    <>
      <div className={styles.gameWrapper}>
        <div className={styles.container}>
          <h2 className={styles.title}>Game</h2>
          <div className={styles.errorsContainer}>
            <p className={styles.errorsText}>Errors: </p>
            <p className={styles.errorsText}>
              {errorsQty}/{maxErrors}
            </p>
          </div>
          <div className={styles.gameContainer}>
            <Grid data={gameData?.cells} handleErrorIncrease={handleErrorIncrease} />
          </div>
        </div>
      </div>
      <Modal isOpen={isGameLost} id="game-lost-modal">
        <div className={styles.modalWrapper}>
          <h2 className={styles.modalHeading}>Sorry, you have reached maximum of errors.</h2>
          <p className={styles.desc}>
            To continue game press 'Continue' to finish game press 'Finish'
          </p>
          <div className={styles.btnsContainer}>
            <CustomButton text="Continue" handleOnClick={() => setIsGameLost(false)} />
            <CustomButton text="Finish" handleOnClick={() => setIsGameLost(false)} isSecondary />
          </div>
        </div>
      </Modal>
    </>
  );
}

export default GamePage;
