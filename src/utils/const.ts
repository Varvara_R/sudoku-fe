export const USER_DATA = "userData";

export const ACCEPT_HEADER_CONF = {
    headers: {
        Accept: 'application/json',
    }
};

export const NO_CORS_HEADER = {
    mode: 'no-cors',
}