const uiErrorMetricsCached: string[] = [];

const checkCachedErrorsKeys = (key: string): boolean => {
  if (!uiErrorMetricsCached.includes(key)) {
    uiErrorMetricsCached.push(key);
    return false;
  }
  return true;
};

export const sendUIErrorMetrics = async (
  errorMessage: string,
  status: number,
  bodyData?: unknown
): Promise<void> => {
  const isErrorCahed = checkCachedErrorsKeys(`${status} ${errorMessage}`);
  if (!isErrorCahed) {
    await fetch('', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify([
        {
          metric: {
            name: 'ui_errors',
            labels: status === 504 ? { GatewayTimeout: '504' } : {},
            value: 1,
            type: status === 504 ? 'timeout' : 'error',
          },
          log: {
            level: 'error',
            message: `${errorMessage} ${bodyData ? `Data ${JSON.stringify(bodyData)}` : ''}`,
          },
        },
      ]),
    });
  }
};
