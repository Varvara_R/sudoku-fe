export const fetcher = async (url: string, conf: Record<string, unknown>) => {
    const response = await fetch(url, conf);
    
    if(!response.ok) {
        const error = new Error(`${response.status} ${response.statusText}`);
        throw error;
    }

    return response.json();
}