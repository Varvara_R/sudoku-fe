import useSWRImmutable from 'swr';
import { ACCEPT_HEADER_CONF, NO_CORS_HEADER } from '../utils/const';
import { customFetcher } from './customFetcher';
import { sendUIErrorMetrics } from '../utils/sendUIErrorMetrics';
import { environment } from '../environment/environment';
import { SuspenseResponseInterface } from '../models/suspenseInterface';
import { IUser } from '../models/user';
import { IGame } from '../models/game';

export function useUserData<U extends boolean | undefined>(
  useSuspense?: U
):SuspenseResponseInterface<IUser, U> {
  const { data, error, mutate } = useSWRImmutable(
    [`${environment.apiUrl}/user/:Adam`, ACCEPT_HEADER_CONF, NO_CORS_HEADER],
    customFetcher,
    {suspense: useSuspense, revalidateOnFocus: false, revalidateOnReconnect: false}
  ) as SuspenseResponseInterface<IUser, U>;

  return { data, error, mutate } as SuspenseResponseInterface<IUser, U>;
}

export function useNewGame<U extends boolean | undefined>(useSuspense?: U): SuspenseResponseInterface<IGame, U> {
  const { data, error, mutate } = useSWRImmutable(
    [`${environment.apiUrl}/newGame`, ACCEPT_HEADER_CONF, NO_CORS_HEADER],
    customFetcher,
    {suspense: useSuspense, revalidateOnFocus: false, revalidateOnReconnect: false}
  );

  return { data, error, mutate } as SuspenseResponseInterface<IGame, U>;
}
