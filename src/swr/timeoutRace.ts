import { sendUIErrorMetrics } from '../utils/sendUIErrorMetrics';

interface ITimeoutRace {
  promise: Promise<Response>;
  url: string;
  method: string;
  body?: unknown;
}

export interface ICustomError extends Error {
  status: number;
  info?: string;
  statusText?: string;
}

export const timeoutRace = async ({ promise, url, method, body }: ITimeoutRace) => {
  let timeoutId;

  try {
    const timeoutPromise = new Promise((resolve, reject) => {
      timeoutId = setTimeout(() => {
        const error = new Error(`Gateway timeout for ${method} ${url}`) as ICustomError;
        error.status = 504;
        error.statusText = `Gateway timeout for ${url}`;
        reject(error);
      }, 10000);
    });
    return (await Promise.race([promise, timeoutPromise])) as Response;
    
  } catch (err) {
    const error = new Error(`Gateway timeout for ${method} ${url}`) as ICustomError;
    error.status = 504;
    if (method === 'POST') {
      sendUIErrorMetrics(`Gateway timeout for ${method} ${url}`, 504, body);
    }
    throw error;
  } finally {
    clearTimeout(timeoutId);
  }
};

export function CustomError(): ICustomError {
  return {
    name: '',
    message: '',
    stack: '',
    status: 0,
    info: '',
  }
}
