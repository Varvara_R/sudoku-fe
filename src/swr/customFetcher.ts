import { ICustomError, timeoutRace } from './timeoutRace';

export const customFetcher = async (url: string, conf: Record<string, unknown> = {}) => {
  const response = await timeoutRace({ promise: fetch(url, conf), method: 'GET', url });

  if (!response.ok) {
    const error = new Error(
      `An error occured while fetching the data: ${response.statusText}`
    ) as ICustomError;
    error.status = response.status;
    throw error;
  }

  const data = await response.json();

  //class-validator check add here:
  // const val = plainToClass(cls, data);
  // await validateOrReject(val as Record<string, unknown>);
  return data;
};
