import useSWR from "swr"
import { USER_DATA } from "../utils/const"


export function useGameData() {
    const emptyResponse = {
        type: undefined,
        data: undefined,
        progress: undefined,
    }

    const {data, mutate} = useSWR(USER_DATA, null, {
        fallbackData: emptyResponse,
    })

    if (!data) {
        return {data: emptyResponse, mutate}
    }

    return {data, mutate}
}