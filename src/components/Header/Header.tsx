import React from 'react';
import { useUserData } from '../../swr/be-data';
import styles from './Header.module.scss';
import logo from '../../images/sudoku_comic_sans_small.jpg'
import { Link } from 'react-router-dom';

function Header(): React.ReactElement | null {
  const { data: userData } = useUserData();

  return (
    <header className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.logoSide}>
          <Link to="/"><img src={logo} alt="Sudoku logo go to home page" className={styles.logo} /></Link>
        </div>
        <div className={styles.navigation}>
          <nav></nav>
        </div>
        <div className={styles.userSide}>
          <p className={styles.profile}>{userData?.firstName} {userData?.lastName}</p>
        </div>
      </div>
    </header>
  );
}

export default Header;
