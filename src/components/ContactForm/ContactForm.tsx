import React from 'react';
import styles from './ContactForm.module.scss';
import CustomInput from '../CustomInput/CustomInput';
import CustomTextArea from '../CustomTextArea/CustomTextArea';
import CustomButton from '../CustomButton/CustomButton';

function ContactForm(): React.ReactElement | null {
  const handleSubmitForm = (e: any) => {
    e.preveventDefault();
  };
  

  return (
    <div className={styles.contactSection}>
      <div className={styles.container}>
        <h3 className={styles.contactTitle}>Feedback form</h3>
        <div className={styles.form}>
          <form onSubmit={handleSubmitForm}>
            <CustomInput label="Name" />
            <CustomInput label="Email"/>
            <CustomTextArea label="Feedback"/>
            <CustomButton text='Submit' />
          </form>
        </div>
      </div>
    </div>
  );
}

export default ContactForm;
