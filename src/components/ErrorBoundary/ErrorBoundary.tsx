import React, { ErrorInfo, ReactNode } from "react";
import { CustomError, ICustomError } from "../../swr/timeoutRace";
import ErrorView from "../ErrorView/ErrorView";

interface Props {
    children: ReactNode;
}

interface State {
    hasError: boolean;
    error: ICustomError;
}

class ErrorBoundary extends React.Component<Props, State> {
    constructor(props: Props, _: State) {
        super(props)
        this.state = { hasError: false, error: CustomError() }
    }

    public static getDerivedStateFromError(err: ICustomError): State {
        return { hasError: true, error: err };
    }

    public componentDidCatch(error: ICustomError, errorInfo: ErrorInfo): void {
        if(this.state.error.status > 0 && this.state.error.status > error.status) {
            this.setState({hasError: true, error })
        }
        if(this.state.error.status === 0) {
            this.setState({ hasError: true, error })
        }
        console.log('Uncaugh error:', error, errorInfo)
    }

    public render() {
        if(this.state.hasError) {
            return (
                <ErrorView errorStatus={this.state.error.status} errorMessage={this.state.error.message} />
            )
        }
        return this.props.children;
    }
}

export default ErrorBoundary;