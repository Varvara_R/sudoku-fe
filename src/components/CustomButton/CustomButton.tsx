import React from 'react';
import styles from './CustomButton.module.scss';

interface ICustomButtonProps {
  text: string;
  handleOnClick?: () => void;
  isSecondary?: boolean;
}

function CustomButton({
  text,
  isSecondary,
  handleOnClick,
}: ICustomButtonProps): React.ReactElement | null {
  return (
    <button
      type="button"
      onClick={handleOnClick}
      onKeyDown={(e) => (e.code === 'Enter' && handleOnClick ? handleOnClick() : undefined)}
      className={`${styles.container} ${isSecondary ? styles.secondary : ''}`}
    >
      {text}
    </button>
  );
}

export default CustomButton;
