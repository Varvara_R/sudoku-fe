import React from 'react';
import styles from './CustomInput.module.scss';

interface ICustomInputProps {
    label: string;
}

function CustomInput({label}: ICustomInputProps): React.ReactElement | null {
    return (
        <div className={styles.customInput}>
            <input className={styles.input} />
            <label>Name</label>
        </div>
    )
}

export default CustomInput;