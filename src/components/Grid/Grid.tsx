import styles from './Grid.module.scss';
import { Row } from '../../models/game';
import Rows from './Rows/Rows';

interface IGridProps {
  data: Row[] | undefined;
  handleErrorIncrease: () => void;
}
function Grid({ data, handleErrorIncrease }: IGridProps): React.ReactElement | null {
  return (
    <div className={styles.gridContainer}>
      <div className={styles.tableContainer}>
        <table className={styles.table}>
          <caption>Sudoku of a day</caption>
          <colgroup className={styles.colgroup}>
            <col></col>
            <col></col>
            <col></col>
          </colgroup>
          <colgroup className={styles.colgroup}>
            <col></col>
            <col></col>
            <col></col>
          </colgroup>
          <colgroup className={styles.colgroup}>
            <col></col>
            <col></col>
            <col></col>
          </colgroup>
          <tbody className={styles.colgroup}>
            {data?.map((row, index) => (
              <Rows key={`${row.toString()}-${index}`} data={row} handleErrorIncrease={handleErrorIncrease}/>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Grid;
