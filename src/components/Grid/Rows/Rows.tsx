import React from 'react';
import { ICell } from '../../../models/game';
import Cell from '../Cell/Cell';
import styles from './Rows.module.scss'

interface IRowsProps {
  data: ICell[] | undefined;
  handleErrorIncrease: () => void;
}

function Rows({ data, handleErrorIncrease }: IRowsProps): React.ReactElement | null {
  return (
    <tr className={styles.row}>
      {data?.map((item, index) => (
        <Cell key={`${item.digit}-${item.isFixed}-${index}`} cell={item} handleErrorIncrease={handleErrorIncrease}/>
      ))}
    </tr>
  );
}

export default Rows;
