import React, { useState } from 'react';
import { ICell } from '../../../models/game';
import styles from './Cell.module.scss';
import classNames from 'classnames';

interface ICellProps {
  cell: ICell;
  handleErrorIncrease: () => void;
}
function Cell({ cell, handleErrorIncrease }: ICellProps): React.ReactElement | null {
  const initialValue = cell.isFixed ? cell.digit.toString() : '';
  const [value, setValue] = useState<string>(initialValue);
  const [isValueError, setIsValueError] = useState(false);

  const handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!cell.isFixed) {
      const newValue = e.target.value;
      if (newValue.trim() !== '' && Number(newValue) !== cell.digit) {
        handleErrorIncrease();
        setIsValueError(true);
      } else {
        setIsValueError(false);
      }
      setValue(newValue);
    }
  };

  const inputStyles = classNames({
    [styles.input]: true,
    [styles.fixedInput]: cell.isFixed,
    [styles.incorrectValue]: isValueError,
  });

  return (
    <td className={styles.cell}>
      <input
        type="text"
        pattern="\d*"
        maxLength={1}
        onChange={(e) => handleChangeValue(e)}
        // onInput={(e) => handleOnInput(e)}
        value={value}
        className={inputStyles}
      />
    </td>
  );
}

export default Cell;
