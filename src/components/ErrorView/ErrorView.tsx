import React, { useEffect } from "react";
import styles from './ErrorView.module.scss'
import { sendUIErrorMetrics } from "../../utils/sendUIErrorMetrics";

interface IErrorViewProps {
    errorStatus: number;
    errorMessage: string;
}

function ErrorView({ errorStatus, errorMessage }: IErrorViewProps): React.ReactElement | null {

    useEffect(() => {
        sendUIErrorMetrics(errorMessage, errorStatus)
    }, []);
    
    return (
        <div>
            <h1>Ooops! There is an error occured</h1>
        </div>
    )
}

export default ErrorView;