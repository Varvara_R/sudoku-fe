import React from 'react';
import styles from './Footer.module.scss';

function Footer(): React.ReactElement | null {
  return (
    <footer className={styles.footerSection}>
      <div className={styles.footerContainer}>
        <div className={styles.contacts}>

        </div>
        <div className={styles.copyright}>

        </div>
      </div>
    </footer>
  );
}

export default Footer;
