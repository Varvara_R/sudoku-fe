import React from 'react';
import styles from './InstructionView.module.scss';

function InstructionView(): React.ReactElement | null {
  return (
    <div className={styles.instructionSection}>
      <div className={styles.rulesContainer}>
        <div className={styles.rules}>
          <p> Here will be some rules</p>
        </div>
        <div className={styles.samples}>
          <p>Here will be some nice pictures with samples of game</p>
        </div>
      </div>
    </div>
  );
}

export default InstructionView;
