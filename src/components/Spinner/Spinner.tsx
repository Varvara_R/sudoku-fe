import React from 'react';
import styles from './Spinner.module.scss';

function Spinner(): React.ReactElement | null {
  return (
    <div className={styles.spinnerWrapper}>
      <div className={styles.svgImage}>
        <svg className={styles.svgImageCircle} viewBox="25 25 50 50">
          <circle cx="50" cy="50" r="20" fill="none" stroke="#d0d0d0" strokeWidth="4" />
          <circle
            className={styles.svgImageCirclePath}
            cx="50"
            cy="50"
            r="20"
            fill="none"
            stroke="#0d1b2a"
            strokeWidth="4"
          />
        </svg>
      </div>
    </div>
  );
}

export default Spinner;