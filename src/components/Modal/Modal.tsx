import classNames from 'classnames';
import React, {ReactElement} from 'react';
import styles from './Modal.module.scss';

interface IModalProps {
    children: ReactElement;
    isOpen: boolean;
    id: string;
}

function Modal({children, isOpen, id}: IModalProps): ReactElement | null {
    const modalContainerStyles = classNames({
        [styles.modalContainer]: true,
        [styles.modalContainerOpen]: isOpen,
    });
    const modalBodyStyles = classNames({
        [styles.modalBody]: true,
        [styles.modalBodyOpen]: isOpen,
    })

    return (
        <div className={modalContainerStyles} id={id}>
            <div className={styles.backdrop} />
            <div role='dialog' aria-modal="true" className={modalBodyStyles}>
               {children}
            </div>
        </div>
    )
}

export default Modal;