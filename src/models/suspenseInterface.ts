import { KeyedMutator } from "swr";
import { ICustomError } from "../swr/timeoutRace";

export type WithSuspenseResponse<T> = {
    data: T;
    mutate: KeyedMutator<T>;
    error: ICustomError;
    isValidating: boolean;
}

export type WithoutSuspenseResponse<T> = {
    data?: T;
    mutate: KeyedMutator<T>;
    error: ICustomError;
    isValidating: boolean;
}

export type SuspenseResponseInterface<T, U extends boolean | undefined> = U extends boolean
  ? WithSuspenseResponse<T>
  : WithoutSuspenseResponse<T>;
