export interface IGame {
    id: number,
    difficulty: string,
    cells: Row[],
}

export type Row = ICell[]

export interface ICell {
    digit: number;
    isFixed: boolean;
}